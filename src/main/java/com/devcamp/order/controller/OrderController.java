package com.devcamp.order.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import com.devcamp.order.model.CCustomer;
import com.devcamp.order.model.COrder;
import com.devcamp.order.repository.ICustomerRepository;
import com.devcamp.order.repository.IOrderRepository;
@CrossOrigin
@RestController
@RequestMapping("/")
public class OrderController {
	@Autowired
	ICustomerRepository pCustomerRepository;

	@Autowired
	IOrderRepository pOrderRepository;

	@GetMapping("/devcamp-users")
	public ResponseEntity<List<CCustomer>> getAllCustomer() {
		try {
			List<CCustomer> pCustomers = new ArrayList<CCustomer>();

			pCustomerRepository.findAll().forEach(pCustomers::add);

			return new ResponseEntity<>(pCustomers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/devcamp-orders")
	public ResponseEntity<Set<COrder>> getOrdersByUserId(@RequestParam(value = "userId") String userId) {
        try {
        	long vUserId = Long.parseLong(userId);
            CCustomer vCustomer = pCustomerRepository.findById(vUserId);
            
            if(vCustomer != null) {
            	return new ResponseEntity<>(vCustomer.getOrders(), HttpStatus.OK);
            } else {
            	return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
