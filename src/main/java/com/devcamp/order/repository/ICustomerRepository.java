package com.devcamp.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.order.model.CCustomer;


public interface ICustomerRepository extends JpaRepository<CCustomer, Long> {
	CCustomer findById(long id); 

}
